
/**
 *
 * @author 032MitroshkinAM
 */
import ru.mam.multyQuery.service.DbStorage;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import ru.mam.multyQuery.model.DataObject;
import ru.mam.multyQuery.model.JoinKey;
import ru.mam.multyQuery.model.JoinType;
import ru.mam.multyQuery.model.RowData;
import ru.mam.multyQuery.model.Title;
import ru.mam.multyQuery.service.ExcelWriteUtil;
import ru.mam.multyQuery.service.LeftJoinData;
import ru.mam.multyQuery.service.SqlUtil;

public class TestReport {

    private static final String SQL_NVP = "select pens.fa \"Фамилия\", pens.im \"Имя\",pens.ot \"Отчество\", pens.npers \"Страховой номер\",pens.rdat \"Дата рождения\", pens.ra \"Район\", uhod.fa \"Фамилия ухаживающего лица\", uhod.im \"Имя ухаживающего лица\", uhod.ot \"Отчество ухаживающего лица\", uhod.npers \"Страховой номер ухаживающего лица\", uhod.rdat \"Дата рождения ухаживающего лица\", 'Прекращение КТЛ в связи с новым назначением' as \"Причина запуска\"\n"
            + "from pf.man pens\n"
            + "inner join pf.pe pe_pens on pens.id=pe_pens.id and pe_pens.dat=(select max(dat) from pf.pe where id=pe_pens.id)\n"
            + "	and pe_pens.np not in ('ПРЕ','СНЯ') and pe_pens.pgp=1 and pe_pens.oper_uh not in ('ПРЕ','СНЯ','ПРИ')\n"
            + "inner join pf.pe pe_uhod on pe_pens.id_uhod=pe_uhod.id and pe_uhod.dat=(select max(dat) from pf.pe where id=pe_uhod.id)\n"
            + "	and pe_uhod.np not in ('ПРЕ','СНЯ') and pe_uhod.pgp=1 and nvl(pe_uhod.otkaz,0)=0\n"
            + "inner join pf.man uhod on pe_pens.id_uhod=uhod.id\n"
            + "where pe_pens.nuhod=1\n"
            + "and uhod.npers = '129-088-365 85'\n"
            + "order by uhod.ra,uhod.npers,uhod.fa,uhod.im,uhod.ot";

    private static final String SQL_WF = "SELECT distinct PERSNUM \"Страховой номер ухаживающего лица\"\n"
            + "FROM (\n"
            + "SELECT (case \n"
            + "	when proc.id_department in (1,29,61,66,67,70) then 1 \n"
            + "	when proc.id_department in (0) then 0 \n"
            + "	when proc.id_department in (2) then 2 \n"
            + "	when proc.id_department in (6) then 6 \n"
            + "	when proc.id_department in (3,26,59) then 3 \n"
            + "	when proc.id_department in (4,34,37,39,62) then 4 \n"
            + "	when proc.id_department in (5,17,35,44,64) then 5 \n"
            + "	when proc.id_department in (7,51) then 7 \n"
            + "	when proc.id_department in (8,20,58,68,73,74) then 8 \n"
            + "	when proc.id_department in (9,10,11,12,13) then 9 \n"
            + "	when proc.id_department in (16,33,55) then 16 \n"
            + "	when proc.id_department in (19,25,49,50,53) then 19 \n"
            + "	when proc.id_department in (21,46,54,56) then 54 \n"
            + "	when proc.id_department in (22,31,36,41) then 31 \n"
            + "	when proc.id_department in (23,42,47,65,72) then 72 \n"
            + "	when proc.id_department in (24,38,60,69) then 38 \n"
            + "	when proc.id_department in (28,32,57,63) then 63 \n"
            + "	when proc.id_department in (40,43,48,71) then 43 \n"
            + "	when proc.id_department in (75) then 75 \n"
            + "else proc.id_department end ) mru, \n"
            + "proc.id_department as raion, genInf.PERSNUM, genInf.FAMILY, genInf.NAME, genInf.PATR\n"
            + ",(SELECT VALUE_VAR FROM DB2ADMIN.ATTRIBUTES atr JOIN DB2ADMIN.VARIABLES var on var.ID_VAR = atr.ID_VAR and var.NAME_VAR = 'Результат рассмотрения' WHERE atr.ID_PROCESS = proc.ID_PROCESS) as result_rasm\n"
            + ",(SELECT VALUE_VAR FROM DB2ADMIN.ATTRIBUTES atr JOIN DB2ADMIN.VARIABLES var on var.ID_VAR = atr.ID_VAR and var.NAME_VAR = 'Дата принятия решения в ЭВД' WHERE atr.ID_PROCESS = proc.ID_PROCESS) as date_resh\n"
            + "FROM DB2ADMIN.PROCESSES proc\n"
            + "JOIN DB2ADMIN.PROCESS_GENERAL_INFO genInf on genInf.ID_PROCESS = proc.ID_PROCESS\n"
            + "JOIN (SELECT task.ID_PROCESS, task.ID_TASK, stagesFrom.DESCRIPTION_STAGE as stage_From, stagesTo.DESCRIPTION_STAGE as stage_to, task.TYPE_TRANSACTION\n"
            + ",task.DATEOFCOMMING, task.DATEOFTAKING, task.DATEOFCOMPLATION, task.ID_USER, task.TYPE_COMPLATION, task.ID_DEPARTMENT\n"
            + "FROM DB2ADMIN.TASKS task\n"
            + "JOIN DB2ADMIN.TYPE_PROCESS typeProc on typeProc.ID_TYPE_PROCESS = task.ID_TYPE_PROCESS and typeProc.DESCRIPTION_PROCESS IN ('НВП1','НВП (типовая схема) ВВ')\n"
            + "JOIN DB2ADMIN.STAGES stagesFrom on stagesFrom.ID_STAGE = task.ID_STAGE_FROM\n"
            + "JOIN DB2ADMIN.STAGES stagesTo on stagesTo.ID_STAGE = task.ID_STAGE_TO\n"
            + "WHERE stagesFrom.DESCRIPTION_STAGE like 'Руководитель ТО ПФР' \n"
            + "and stagesTo.DESCRIPTION_STAGE like 'Выплата%'\n"
            + "and MONTH(task.DATEOFCOMMING) = MONTH(CURRENT_DATE) and YEAR(task.DATEOFCOMMING) = YEAR(CURRENT_DATE)) stages on stages.ID_PROCESS = proc.ID_PROCESS\n"
            + "WHERE proc.ID_STATUS <> 4\n"
            + "and date(proc.DATEOFCOMMING) between '@date1' and '@date2'\n"
            + "and genInf.PERSNUM = '129-088-365 85'\n"
            + ") temp\n"
            + "WHERE temp.result_rasm = 'УДОВЛЕТВОРЕНО' and temp.date_resh is not null\n"
            + "and ((mru=@mru and raion=@raion) or mru=@mru or raion=@raion or (@mru=0 AND @raion=0))";

    public TestReport() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

//    @Test
    public void testExecRep() {
        try (Connection wfConn = DbStorage.getWf().getConnection();
                Connection rosConn = DbStorage.getRos61().getConnection();
                Statement wfStatement = wfConn.createStatement();
                Statement rosStatement = rosConn.createStatement();) {

            ResultSet rosRS = rosStatement.executeQuery(SQL_NVP);
            List<Title> rosTitle = SqlUtil.readTitleFromMetaData(rosRS.getMetaData());
            Map<JoinKey, List<RowData>> rosRows = SqlUtil.readRowsFromResultSet(rosRS, rosTitle, Arrays.asList("Страховой номер ухаживающего лица"));

            String wfQuery = SQL_WF.replace("@date1", "01.06.2021").replace("@date2", "12.07.2021");
            wfQuery = wfQuery.replace("@mru", "0");
            wfQuery = wfQuery.replace("@raion", "0");
            ResultSet wfRS = wfStatement.executeQuery(wfQuery);
            List<Title> wfTitle = SqlUtil.readTitleFromMetaData(wfRS.getMetaData());
            Map<JoinKey, List<RowData>> wfRows = SqlUtil.readRowsFromResultSet(wfRS, wfTitle, Arrays.asList("Страховой номер ухаживающего лица"));

            LeftJoinData leftJoinData = new LeftJoinData();
            DataObject join = leftJoinData.join(new DataObject(rosTitle, rosRows), new DataObject(wfTitle, wfRows), Arrays.asList("Страховой номер ухаживающего лица"));
//            List<DataObject> dataObjects = new ArrayList<>();
//            dataObjects.add(new DataObject(rosTitle, rosRows));
//            dataObjects.add(new DataObject(wfTitle, wfRows));
            InputStream itogExcel = ExcelWriteUtil.createItogExcel(join);
            File res = new File("D:\\reportCenter.xls");
            copyInputStreamToFile(itogExcel, res);
        } catch (Exception ex) {
            System.out.println(ex);
        }
    }

    private static void copyInputStreamToFile(InputStream inputStream, File file) throws IOException {
        try (FileOutputStream outputStream = new FileOutputStream(file, false)) {
            int read;
            byte[] bytes = new byte[8192];
            while ((read = inputStream.read(bytes)) != -1) {
                outputStream.write(bytes, 0, read);
            }
        }
    }

}
