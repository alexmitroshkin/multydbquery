package ru.mam.multyQuery.service;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import ru.mam.multyQuery.model.CellData;
import ru.mam.multyQuery.model.DataObject;
import ru.mam.multyQuery.model.JoinKey;
import ru.mam.multyQuery.model.RowData;
import ru.mam.multyQuery.model.Title;
import ru.mam.multyQuery.model.ValueType;

/**
 *
 * @author 032MitroshkinAM
 */
public class LeftJoinDataTest {

    public LeftJoinDataTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of join method, of class LeftJoinData.
     */
//    @Test
    public void testLeftJoin() {
        System.out.println("start join");
        try (Connection csConn = DbStorage.getCs().getConnection();
                Connection wfConn = DbStorage.getWf().getConnection();
                Statement csStatement = csConn.createStatement();
                Statement wfStatement = wfConn.createStatement();
                FileOutputStream fos = new FileOutputStream(new File("D:/leftJoin.xlsx"));) {
            ResultSet csRs = csStatement.executeQuery("SELECT CAST(SYSTEM_NUM as bigint) AS ID_PROCESS, REGISTER_DATE, PERSNUM FROM DB2ADMIN.R_REGISTER_EXT WHERE REGISTER_DATE between '01.08.2021' and '02.08.2021' and SYSTEM_NAME LIKE 'ПУП' order by ID_PROCESS");
            List<Title> csTitle = SqlUtil.readTitleFromMetaData(csRs.getMetaData());
            Map<JoinKey, List<RowData>> csMap = SqlUtil.readRowsFromResultSet(csRs, csTitle, Arrays.asList("PERSNUM"));
            DataObject dataObject1 = new DataObject(csTitle, csMap);

            ResultSet wfRs = wfStatement.executeQuery("SELECT proc.ID_PROCESS, DATEOFCOMMING, DATEOFCOMPLETION, ID_TYPE_PROCESS, ID_DEPARTMENT, REPLACE(REPLACE(PERSNUM, '-', ''), ' ', '') as PERSNUM\n"
                    + "FROM DB2ADMIN.PROCESSES proc\n"
                    + "JOIN DB2ADMIN.PROCESS_GENERAL_INFO procGenInf on proc.ID_PROCESS = procGenInf.ID_PROCESS\n"
                    + "WHERE DATEOFCOMMING between '2021-08-01 00:00:00.000' and '2021-08-02 23:59:59.000'");
            List<Title> wfTitle = SqlUtil.readTitleFromMetaData(wfRs.getMetaData());
            Map<JoinKey, List<RowData>> wfMap = SqlUtil.readRowsFromResultSet(wfRs, wfTitle, Arrays.asList("PERSNUM"));
            DataObject dataObject2 = new DataObject(wfTitle, wfMap);

            LeftJoinData instance = new LeftJoinData();
            DataObject result = instance.join(dataObject1, dataObject2, Arrays.asList("PERSNUM"));
            InputStream createItogExcel = ExcelWriteUtil.createItogExcel(result);
            byte[] buffer = new byte[createItogExcel.available()];
            createItogExcel.read(buffer);
            fos.write(buffer);
            System.out.println("end join");
        } catch (SQLException | IOException ex) {
            Logger.getLogger(LeftJoinDataTest.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

//    @Test
    public void testInnerJoin() {
        System.out.println("start join");
        try (Connection csConn = DbStorage.getCs().getConnection();
                Connection wfConn = DbStorage.getWf().getConnection();
                Statement csStatement = csConn.createStatement();
                Statement wfStatement = wfConn.createStatement();
                FileOutputStream fos = new FileOutputStream(new File("D:/innerJoin.xlsx"));) {
            ResultSet csRs = csStatement.executeQuery("SELECT CAST(SYSTEM_NUM as bigint) AS ID_PROCESS, REGISTER_DATE, PERSNUM FROM DB2ADMIN.R_REGISTER_EXT WHERE REGISTER_DATE between '01.08.2021' and '02.08.2021' and SYSTEM_NAME LIKE 'ПУП' order by ID_PROCESS");
            List<Title> csTitle = SqlUtil.readTitleFromMetaData(csRs.getMetaData());
            Map<JoinKey, List<RowData>> csMap = SqlUtil.readRowsFromResultSet(csRs, csTitle, Arrays.asList("PERSNUM"));
            DataObject dataObject1 = new DataObject(csTitle, csMap);

            ResultSet wfRs = wfStatement.executeQuery("SELECT proc.ID_PROCESS, DATEOFCOMMING, DATEOFCOMPLETION, ID_TYPE_PROCESS, ID_DEPARTMENT, REPLACE(REPLACE(PERSNUM, '-', ''), ' ', '') as PERSNUM\n"
                    + "FROM DB2ADMIN.PROCESSES proc\n"
                    + "JOIN DB2ADMIN.PROCESS_GENERAL_INFO procGenInf on proc.ID_PROCESS = procGenInf.ID_PROCESS\n"
                    + "WHERE DATEOFCOMMING between '2021-08-01 00:00:00.000' and '2021-08-02 23:59:59.000'");
            List<Title> wfTitle = SqlUtil.readTitleFromMetaData(wfRs.getMetaData());
            Map<JoinKey, List<RowData>> wfMap = SqlUtil.readRowsFromResultSet(wfRs, wfTitle, Arrays.asList("PERSNUM"));
            DataObject dataObject2 = new DataObject(wfTitle, wfMap);

            InnerJoinData instance = new InnerJoinData();
            DataObject result = instance.join(dataObject1, dataObject2, Arrays.asList("PERSNUM"));
            InputStream createItogExcel = ExcelWriteUtil.createItogExcel(result);
            byte[] buffer = new byte[createItogExcel.available()];
            createItogExcel.read(buffer);
            fos.write(buffer);
            System.out.println("end join");
        } catch (SQLException | IOException ex) {
            Logger.getLogger(LeftJoinDataTest.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
