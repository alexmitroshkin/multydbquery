package ru.mam.multyQuery.service;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import ru.mam.multyQuery.model.JoinKey;
import ru.mam.multyQuery.model.RowData;
import ru.mam.multyQuery.model.Title;

/**
 *
 * @author 032MitroshkinAM
 */
public class CsvReadUtilTest {
    
    public CsvReadUtilTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of readTitle method, of class CsvReadUtil.
     */
//    @Test
    public void testReadTitle() throws Exception {
        System.out.println("readTitle");
        String csvFile = "D:\\RECALC_309003416\\Recalc309003416_plus.csv";
        Character coldel = ';';
        Character chardel = '"';
        Character space = '=';
        List<Title> expResult = null;
        List<Title> title = CsvReadUtil.readTitle(csvFile, coldel, chardel, space);
        Map<JoinKey, List<RowData>> rows = CsvReadUtil.readRows(csvFile, title, coldel, chardel, space, Arrays.asList("СНИЛС"));
        System.out.println("");
    }
    
}
