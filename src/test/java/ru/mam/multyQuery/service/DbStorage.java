package ru.mam.multyQuery.service;

import com.ibm.db2.jcc.DB2DataSource;
import javax.sql.DataSource;

/**
 *
 * @author 032MitroshkinAM
 */
public class DbStorage {

    private static DataSource wf;
    private static DataSource cs;
    private static DataSource ros61;

    public static DataSource getWf() {
        if (wf == null) {
            DB2DataSource dataSource = new DB2DataSource();
            dataSource.setServerName("sw03200008032t");
            dataSource.setPortNumber(50000);
            dataSource.setDatabaseName("WF");
            dataSource.setUser("db2admin");
            dataSource.setPassword("db2admin");
            wf = dataSource;
        }
        return wf;
    }

    public static DataSource getCs() {
        if (cs == null) {
            DB2DataSource dataSource = new DB2DataSource();
            dataSource.setServerName("sw03200008032t");
            dataSource.setPortNumber(50000);
            dataSource.setDatabaseName("CSERVICE");
            dataSource.setUser("db2admin");
            dataSource.setPassword("db2admin");
            cs = dataSource;
        }
        return cs;
    }

    public static DataSource getRos61() {
        if (ros61 == null) {
            DB2DataSource dataSource = new DB2DataSource();
            dataSource.setServerName("sw03200008061");
            dataSource.setPortNumber(50000);
            dataSource.setDatabaseName("ROS");
            dataSource.setUser("db2admin");
            dataSource.setPassword("db2admin");
            ros61 = dataSource;
        }
        return ros61;
    }

}
