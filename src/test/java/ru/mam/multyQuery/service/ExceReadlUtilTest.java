package ru.mam.multyQuery.service;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import ru.mam.multyQuery.model.JoinKey;
import ru.mam.multyQuery.model.RowData;
import ru.mam.multyQuery.model.Title;

/**
 *
 * @author 032MitroshkinAM
 */
public class ExceReadlUtilTest {

    public ExceReadlUtilTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of readTitleFromExcel method, of class ExceReadlUtil.
     */
//    @Test
    public void testReadTitleFromExcel() {
        System.out.println("readTitleFromExcel");
        try (XSSFWorkbook workbook = new XSSFWorkbook("D:\\Файл МИЦ 20.07.2021 - копия.xlsx")) {
            Sheet sheet = workbook.getSheetAt(0);
            List<Title> result = ExcelReadlUtil.readTitleFromExcel(sheet);
            System.out.println("");
        } catch (IOException ex) {
            Logger.getLogger(ExceReadlUtilTest.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Test of readTitleFromRow method, of class ExceReadlUtil.
     */
//    @Test
    public void testReadTitleFromRow() {
        System.out.println("readTitleFromRow");
        try (XSSFWorkbook workbook = new XSSFWorkbook("D:\\RRYYYYMMDD_sved приложение 27.xlsx")) {
            Sheet sheet = workbook.getSheetAt(0);
            List<Title> result = ExcelReadlUtil.readTitleFromExcel(sheet, 7, 9);
            System.out.println("");
        } catch (IOException ex) {
            Logger.getLogger(ExceReadlUtilTest.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Test of readRowsFromXls method, of class ExceReadlUtil.
     */
//    @Test
    public void testReadRowsFromXls() {
        try {
            System.out.println("readRowsFromXls");
            Sheet sheet = null;
            List<Title> title = null;
            List<String> joinKeysLabel = null;
            Map<JoinKey, List<RowData>> expResult = null;
            Map<JoinKey, List<RowData>> result = ExcelReadlUtil.readRowsFromXls(sheet, title, joinKeysLabel);
            assertEquals(expResult, result);
            // TODO review the generated test code and remove the default call to fail.
            fail("The test case is a prototype.");
        } catch (IOException ex) {
            Logger.getLogger(ExceReadlUtilTest.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
