package ru.mam.multyQuery.service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import ru.mam.multyQuery.model.CellData;
import ru.mam.multyQuery.model.JoinKey;
import ru.mam.multyQuery.model.RowData;
import ru.mam.multyQuery.model.Title;
import ru.mam.multyQuery.model.ValueType;

/**
 *
 * @author 032MitroshkinAM
 */
public class ExcelReadlUtil {

    /**
     * Чтение строки заголовка Excel и определение типа данных в столбце. В
     * расчете того что 1 строка: заголовок, 2 строка: данные
     *
     * @param sheet Лист Excel
     * @return
     */
    public static List<Title> readTitleFromExcel(Sheet sheet) {
        return readTitleFromExcel(sheet, 0, 1);
    }

    /**
     * Чтение заголовка Excel и определение типа данных в столбце.
     *
     * @param sheet Лист Excel
     * @param rowNumTitle Номер строки загаловка
     * @param rowNumData Номер строки данных
     * @return
     */
    public static List<Title> readTitleFromExcel(Sheet sheet, int rowNumTitle, int rowNumData) {
        List<Title> result = new ArrayList<>();
        Row rowTitle = sheet.getRow(rowNumTitle);
        for (int column = 0; column < rowTitle.getLastCellNum(); column++) {
            Cell cellTitle = rowTitle.getCell(column);
            for (int rowNum = rowNumData; rowNum <= sheet.getLastRowNum(); rowNum++) {
                Cell cell = sheet.getRow(rowNum).getCell(column);
                if (cell != null && cell.getCellType() != CellType.BLANK && cell.getCellType() != CellType._NONE) {
                    ValueType valueType = ValueType.forTypeXls(cell.getCellType(), cell);
                    result.add(new Title(cellTitle.getStringCellValue(), column, valueType));
                    break;
                }
            }
        }
        return result;
    }

    /**
     * Чтение строк Excel. В расчете того что 2 строка: данные
     *
     * @param sheet Лист Excel
     * @param title Заголовок
     * @param joinKeysLabel Ключи
     * @return
     * @throws java.io.IOException
     */
    public static Map<JoinKey, List<RowData>> readRowsFromXls(Sheet sheet, List<Title> title, List<String> joinKeysLabel) throws IOException {
        return readRowsFromXls(sheet, title, joinKeysLabel, 1);
    }

    /**
     *
     * @param sheet
     * @param title
     * @param joinKeysLabel
     * @param rowNumData
     * @return
     * @throws java.io.IOException
     */
    public static Map<JoinKey, List<RowData>> readRowsFromXls(Sheet sheet, List<Title> title, List<String> joinKeysLabel, int rowNumData) throws IOException {
        Map<JoinKey, List<RowData>> result = new LinkedHashMap<>();
        for (int rownum = rowNumData; rownum <= sheet.getLastRowNum(); rownum++) {
            Row row = sheet.getRow(rownum);
            RowData rowData = new RowData((new ArrayList<>()));
            JoinKey key = new JoinKey(new ArrayList<>());
            for (Title xlsTitle : title) {
                CellData xlsCellDate = new CellData(valueFromXlsByType(row.getCell(xlsTitle.getPosition()), xlsTitle), xlsTitle);
                if (joinKeysLabel.contains(xlsTitle.getLabel())) {
                    key.getKeys().add(xlsCellDate);
                }
                rowData.getCellData().add(xlsCellDate);
            }
            if (result.containsKey(key)) {
                result.get(key).add(rowData);
            } else {
                List<RowData> datas = new ArrayList<>();
                datas.add(rowData);
                result.put(key, datas);
            }
        }
        return result;
    }

    private static Object valueFromXlsByType(Cell cell, Title title) throws IOException {
        if (cell == null) {
            return null;
        }
        try {
            switch (title.getType()) {
                case STRING:
                    return cell.getStringCellValue();
                case DATE:
                    return cell.getDateCellValue();
                case DOUBLE:
                    return cell.getNumericCellValue();
                default:
                    return null;
            }
        } catch (IllegalStateException ex) {
            throw new IOException("Ошибка чтения ячейки: " + cell + ", столбец: " + title.getLabel(), ex);
        }
    }
}
