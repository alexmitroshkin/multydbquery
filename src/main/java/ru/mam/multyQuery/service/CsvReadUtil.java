package ru.mam.multyQuery.service;

import com.opencsv.exceptions.CsvException;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import ru.mam.multyQuery.model.CellData;
import ru.mam.multyQuery.model.JoinKey;
import ru.mam.multyQuery.model.RowData;
import ru.mam.multyQuery.model.Title;
import ru.mam.multyQuery.model.ValueType;

/**
 *
 * @author 032MitroshkinAM
 */
public class CsvReadUtil {

    public static List<Title> readTitle(String csvFile, Character coldel, Character chardel, Character space) throws IOException, CsvException {
        List<Title> result = new ArrayList<>();
        try (BufferedReader br = new BufferedReader(new FileReader(csvFile))) {
            String line;
            if ((line = br.readLine()) != null) {
                String[] values = line.split(coldel.toString());
                for (int i = 0; i < values.length; i++) {
                    char fistrChar = values[i].charAt(0);
                    if (fistrChar == space) {
                        values[i] = values[i].substring(1);
                        if (values[i].charAt(0) == chardel && values[i].charAt(values[i].length() - 1) == chardel) {
                            values[i] = values[i].replaceAll(chardel.toString(), "");
                        }
                    }
                    result.add(new Title(values[i], i, ValueType.STRING));
                }
            }
        }
        return result;
    }

    public static Map<JoinKey, List<RowData>> readRows(String csvFile, List<Title> title, Character coldel, Character chardel, Character space, List<String> joinKeysLabel) throws IOException {
        Map<JoinKey, List<RowData>> result = new LinkedHashMap<>();
        try (BufferedReader br = new BufferedReader(new FileReader(csvFile))) {
            String line;
            br.readLine();
            while ((line = br.readLine()) != null) {
                RowData rowData = new RowData((new ArrayList<>()));
                JoinKey key = new JoinKey(new ArrayList<>());
                String[] values = line.split(coldel.toString());
                for (Title titleItem : title) {
                    if (values[titleItem.getPosition()].length() != 0) {
                        char fistrChar = values[titleItem.getPosition()].charAt(0);
                        if (fistrChar == space) {
                            values[titleItem.getPosition()] = values[titleItem.getPosition()].substring(1);
                            if (values[titleItem.getPosition()].charAt(0) == chardel && values[titleItem.getPosition()].charAt(values[titleItem.getPosition()].length() - 1) == chardel) {
                                values[titleItem.getPosition()] = values[titleItem.getPosition()].replaceAll(chardel.toString(), "");
                            }
                        }
                    }
                    CellData cellData = new CellData(values[titleItem.getPosition()], titleItem);
                    if (joinKeysLabel.contains(titleItem.getLabel())) {
                        key.getKeys().add(cellData);
                    }
                    rowData.getCellData().add(cellData);
                }
                if (result.containsKey(key)) {
                    result.get(key).add(rowData);
                } else {
                    List<RowData> datas = new ArrayList<>();
                    datas.add(rowData);
                    result.put(key, datas);
                }
            }

        }
        return result;
    }
}
