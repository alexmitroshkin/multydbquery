package ru.mam.multyQuery.service;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import ru.mam.multyQuery.model.CellData;
import ru.mam.multyQuery.model.JoinKey;
import ru.mam.multyQuery.model.RowData;
import ru.mam.multyQuery.model.Title;
import ru.mam.multyQuery.model.ValueType;

/**
 *
 * @author 032MitroshkinAM
 */
public class SqlUtil {

    public static List<Title> readTitleFromMetaData(ResultSetMetaData metaData) throws SQLException {
        List<Title> result = new ArrayList<>();
        for (int i = 1; i <= metaData.getColumnCount(); i++) {
            String columnLabel = metaData.getColumnLabel(i);
            ValueType valueType = ValueType.forTypeSql(metaData.getColumnClassName(i));
            result.add(new Title(columnLabel, i, valueType));
        }
        return result;
    }

    public static Map<JoinKey, List<RowData>> readRowsFromResultSet(ResultSet rs, List<Title> title, List<String> joinKeysLabel) throws SQLException {
        Map<JoinKey, List<RowData>> result = new LinkedHashMap<>();
        while (rs.next()) {
            RowData rowData = new RowData(new ArrayList<>());
            JoinKey key = new JoinKey(new ArrayList<>());
            for (Title sqlTitle : title) {
                CellData sqlCellData = new CellData(rs.getObject(sqlTitle.getPosition()), sqlTitle);
//                for (String joinKey : joinKeysLabel) {
//                    if (joinKey.equals(sqlTitle.getLabel())) {
//                        key.getKeys().add(sqlCellData);
//                    }
//                }
                if (joinKeysLabel.contains(sqlTitle.getLabel())) {
                    key.getKeys().add(sqlCellData);
                }
                rowData.getCellData().add(sqlCellData);
            }
            if (result.containsKey(key)) {
                result.get(key).add(rowData);
            } else {
                List<RowData> datas = new ArrayList<>();
                datas.add(rowData);
                result.put(key, datas);
            }
        }
        return result;
    }
}
