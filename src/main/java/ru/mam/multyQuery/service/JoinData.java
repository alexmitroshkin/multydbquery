package ru.mam.multyQuery.service;

import java.util.List;
import ru.mam.multyQuery.model.DataObject;

/**
 *
 * @author 032MitroshkinAM
 */
public interface JoinData {

    public DataObject join(DataObject dataObject1, DataObject dataObject2, List<String> joinKeys);
}
