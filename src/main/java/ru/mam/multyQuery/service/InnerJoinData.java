package ru.mam.multyQuery.service;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import ru.mam.multyQuery.model.CellData;
import ru.mam.multyQuery.model.DataObject;
import ru.mam.multyQuery.model.JoinKey;
import ru.mam.multyQuery.model.RowData;
import ru.mam.multyQuery.model.Title;

/**
 *
 * @author 032MitroshkinAM
 */
public class InnerJoinData implements JoinData {

    @Override
    public DataObject join(DataObject dataObject1, DataObject dataObject2, List<String> joinKeys) {
        List<Title> resultTitles = new ArrayList<>();
        Map<JoinKey, List<RowData>> resultMap = new LinkedHashMap<>();

        List<Title> titles1 = dataObject1.getTitles();
        Map<JoinKey, List<RowData>> data1 = dataObject1.getData();
        List<Title> titles2 = dataObject2.getTitles();
        Map<JoinKey, List<RowData>> data2 = dataObject2.getData();
        RowData emptyRow2 = new RowData(new ArrayList<>());

        resultTitles.addAll(titles1);
        for (Title title : titles2) {
            if (joinKeys.contains(title.getLabel())) {
                continue;
            }
            switch (title.getType()) {
                case STRING:
                    emptyRow2.getCellData().add(new CellData("", title));
                    break;
                case DATE:
                    emptyRow2.getCellData().add(new CellData(null, title));
                    break;
                case DOUBLE:
                    emptyRow2.getCellData().add(new CellData(Double.valueOf(0), title));
                    break;
                case INTEGER:
                    emptyRow2.getCellData().add(new CellData(0, title));
                    break;
                case LONG:
                    emptyRow2.getCellData().add(new CellData(Long.valueOf(0), title));
                    break;
                case TIMESTAMP:
                    emptyRow2.getCellData().add(new CellData(null, title));
                    break;
            }
            resultTitles.add(title);
        }
        for (Map.Entry<JoinKey, List<RowData>> entry : data1.entrySet()) {
            JoinKey key = entry.getKey();
            List<RowData> data1Value = entry.getValue();
            List<RowData> data2Value = data2.get(key);
            List<RowData> resultRow = new ArrayList<>();
            if (data2Value != null) {
                for (RowData rowData1 : data1Value) {
                    for (RowData rowData2 : data2Value) {
                        RowData resRow = new RowData(new ArrayList<>());
                        resRow.getCellData().addAll(rowData1.getCellData());
                        for (CellData cellData : rowData2.getCellData()) {
                            if (joinKeys.contains(cellData.getTitle().getLabel())) {
                                continue;
                            }
                            resRow.getCellData().add(cellData);
                        }
                        resultRow.add(resRow);
                    }
                }
                resultMap.put(key, resultRow);
            }
        }
        DataObject result = new DataObject(resultTitles, resultMap);
        return result;
    }

}
