package ru.mam.multyQuery.service;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Map;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.DataFormat;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import ru.mam.multyQuery.model.CellData;
import ru.mam.multyQuery.model.DataObject;
import ru.mam.multyQuery.model.JoinKey;
import ru.mam.multyQuery.model.Key;
import ru.mam.multyQuery.model.RowData;
import ru.mam.multyQuery.model.Title;
import ru.mam.multyQuery.model.ValueType;

/**
 *
 * @author 032MitroshkinAM
 */
public class ExcelWriteUtil {

    private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("dd.MM.yyyy");
    private static final SimpleDateFormat DATE_TIME_FORMAT = new SimpleDateFormat("dd.MM.yyyy hh:mm:ss");

    public static InputStream createItogExcel(DataObject dataObject) throws IOException {
        InputStream result = null;
        try (HSSFWorkbook workbook = new HSSFWorkbook()) {
            int rowCount = 1;
            int listCount = 1;
            Sheet sheet = workbook.createSheet("Лист" + listCount);
            DataFormat fmt = workbook.createDataFormat();
            CellStyle cellStyle = workbook.createCellStyle();
            cellStyle.setDataFormat(fmt.getFormat("@"));

            writeTitleRow(sheet, dataObject.getTitles(), cellStyle);
            for (Map.Entry<JoinKey, List<RowData>> entry : dataObject.getData().entrySet()) {
                List<RowData> value = entry.getValue();
                for (RowData rowData : value) {
                    if (rowCount == 65536) {
                        listCount++;
                        sheet = workbook.createSheet("Лист" + listCount);
                        writeTitleRow(sheet, dataObject.getTitles(), cellStyle);
                        rowCount = 1;
                    }
                    Row row = sheet.createRow(rowCount);
                    writeExcel(row, cellStyle, rowData);
                    rowCount++;
                }
            }
            ByteArrayOutputStream arrayOutputStream = new ByteArrayOutputStream();
            workbook.write(arrayOutputStream);
            byte[] bs = arrayOutputStream.toByteArray();
            result = new ByteArrayInputStream(bs);
        }
        return result;
    }

//    private static int writeExcel(Sheet sheet, CellStyle cellStyle, List<String> joinKeysLabel, Key key, RowData rowData, List<DataObject> dataObjectList, int rowCount) {
//        Cell cell = null;
//        int cellCount = 0;
//        for (int dataCnt = 1; dataCnt < dataObjectList.size(); dataCnt++) {
//            List<RowData> joinData = dataObjectList.get(dataCnt).getData().get(key);
//            if (joinData != null) {
//                for (RowData rowData1 : joinData) {
//                    Row row = sheet.createRow(rowCount);
//                    for (CellData cellData : rowData.getCellData()) {
//                        cell = row.createCell(cellCount);
//                        cell.setCellStyle(cellStyle);
//                        getValueByType(cell, cellData.getValue(), cellData.getTitle().getType());
//                        cellCount++;
//                    }
//                    for (CellData cellData : rowData1.getCellData()) {
//                        if (joinKeysLabel.contains(cellData.getTitle().getLabel())) {
//                            continue;
//                        }
//                        cell = row.createCell(cellCount);
//                        cell.setCellStyle(cellStyle);
//                        getValueByType(cell, cellData.getValue(), cellData.getTitle().getType());
//                        cellCount++;
//                    }
//                }
//                rowCount++;
//            } else {
//                Row row = sheet.createRow(rowCount);
//                for (CellData cellData : rowData.getCellData()) {
//                    cell = row.createCell(cellCount);
//                    cell.setCellStyle(cellStyle);
//                    getValueByType(cell, cellData.getValue(), cellData.getTitle().getType());
//                    cellCount++;
//                }
//                rowCount++;
//            }
//        }
//        return rowCount;
//    }
    private static void writeExcel(Row row, CellStyle cellStyle, RowData rowData) {
        Cell cell = null;
        int cellCount = 0;
        for (CellData cellData : rowData.getCellData()) {
            cell = row.createCell(cellCount);
            cell.setCellStyle(cellStyle);
            getValueByType(cell, cellData.getValue(), cellData.getTitle().getType());
            cellCount++;
        }

    }

//    private static void writeTitleRow(Sheet sheet, List<List<Title>> titleList, List<String> joinKeysLabel, CellStyle cellStyle) {
//        Row row = sheet.createRow(0);
//        Cell cell = null;
//        int cellCount = 0;
//        for (int dataObjectTitleCount = 0; dataObjectTitleCount < titleList.size(); dataObjectTitleCount++) {
//            for (Title title : titleList.get(dataObjectTitleCount)) {
//                if (dataObjectTitleCount > 0 && joinKeysLabel.contains(title.getLabel())) {
//                    continue;
//                }
//                cell = row.createCell(cellCount);
//                cell.setCellStyle(cellStyle);
//                cell.setCellValue(title.getLabel());
//                cellCount++;
//            }
//        }
//    }
    private static void writeTitleRow(Sheet sheet, List<Title> titles, CellStyle cellStyle) {
        Row row = sheet.createRow(0);
        Cell cell = null;
        int cellCount = 0;
        for (Title title : titles) {
            cell = row.createCell(cellCount);
            cell.setCellStyle(cellStyle);
            cell.setCellValue(title.getLabel());
            cellCount++;
        }
    }

    public static void getValueByType(Cell cell, Object value, ValueType valueType) {
        switch (valueType) {
            case DATE:
                if (value != null) {
                    cell.setCellValue(DATE_FORMAT.format(value));
                } else {
                    cell.setCellValue("");
                }
                break;
            case TIMESTAMP:
                if (value != null) {
                    cell.setCellValue(DATE_TIME_FORMAT.format(value));
                } else {
                    cell.setCellValue("");
                }
                break;
            case DOUBLE:
                if (value != null) {
                    cell.setCellValue((double) value);
                } else {
                    cell.setCellValue(0);
                }
                break;
            case INTEGER:
                if (value != null) {
                    cell.setCellValue((Integer) value);
                } else {
                    cell.setCellValue(0);
                }
                break;
            case STRING:
                if (value != null) {
                    cell.setCellValue((String) value);
                } else {
                    cell.setCellValue("");
                }
                break;
            case LONG:
                if (value != null) {
                    cell.setCellValue((Long) value);
                } else {
                    cell.setCellValue(0);
                }
                break;
            case SHORT:
                if (value != null) {
                    cell.setCellValue((Short) value);
                } else {
                    cell.setCellValue(0);
                }
                break;
        }
    }
}
