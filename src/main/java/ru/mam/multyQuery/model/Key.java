package ru.mam.multyQuery.model;

/**
 *
 * @author 032MitroshkinAM
 */
public interface Key {
    
    public int hashCodeKey();
    
    public boolean equalsKey(Object obj);
}
