package ru.mam.multyQuery.model;

/**
 *
 * @author 032MitroshkinAM
 */
public enum JoinType {
    INNER_JOIN, LEFT_JOIN;
}
