package ru.mam.multyQuery.model;

import java.util.List;
import java.util.Objects;

public class JoinKey implements Key {

    private List<CellData> keys;

    public JoinKey() {
    }

    public JoinKey(List<CellData> keys) {
        this.keys = keys;
    }

    public List<CellData> getKeys() {
        return keys;
    }

    public void setKeys(List<CellData> keys) {
        this.keys = keys;
    }

    @Override
    public boolean equalsKey(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        JoinKey joinKey = (JoinKey) o;
        return Objects.equals(keys, joinKey.keys);
    }

    @Override
    public int hashCodeKey() {
        return Objects.hash(keys);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        JoinKey joinKey = (JoinKey) o;
        return Objects.equals(keys, joinKey.keys);
    }

    @Override
    public int hashCode() {
        return Objects.hash(keys);
    }
}
