package ru.mam.multyQuery.model;

import java.util.List;
import java.util.Map;

public class DataObject {

    private List<Title> titles;
    private Map<JoinKey, List<RowData>> data;

    public DataObject(List<Title> titles, Map<JoinKey, List<RowData>> data) {
        this.titles = titles;
        this.data = data;
    }

    public DataObject() {
    }

    public Map<JoinKey, List<RowData>> getData() {
        return data;
    }

    public void setData(Map<JoinKey, List<RowData>> data) {
        this.data = data;
    }

    public List<Title> getTitles() {
        return titles;
    }

    public void setTitles(List<Title> titles) {
        this.titles = titles;
    }

}
