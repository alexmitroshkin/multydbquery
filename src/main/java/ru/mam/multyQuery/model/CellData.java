package ru.mam.multyQuery.model;

import java.util.Objects;

public class CellData implements Cloneable {

    private Object value;
    private Title title;

    public CellData() {
    }

    public CellData(Object value, Title title) {
        this.value = value;
        this.title = title;
    }

    public Object getValue() {
        return value;
    }

    public void setValue(Object value) {
        this.value = value;
    }

    public Title getTitle() {
        return title;
    }

    public void setTitle(Title title) {
        this.title = title;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        CellData sqlCellData = (CellData) o;
        return Objects.equals(value, sqlCellData.value);
    }

    @Override
    public int hashCode() {
        return Objects.hash(value);
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        return new CellData(value, (Title) title.clone());
    }

}
