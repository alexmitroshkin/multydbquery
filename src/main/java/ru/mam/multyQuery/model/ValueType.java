package ru.mam.multyQuery.model;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.DateUtil;

public enum ValueType {
    DATE("java.sql.Date"),
    TIMESTAMP("java.sql.Timestamp"),
    STRING("java.lang.String"),
    INTEGER("java.lang.Integer"),
    LONG("java.lang.Long"),
    SHORT("java.lang.Short"),
    DOUBLE("java.lang.Double");

    private final String typeSql;

    private ValueType(String typeSql) {
        this.typeSql = typeSql;
    }

    public String getTypeSql() {
        return typeSql;
    }

    public static ValueType forTypeSql(String type) {
        for (ValueType valueType : ValueType.values()) {
            if (valueType.getTypeSql().equals(type)) {
                return valueType;
            }
        }
        return null;
    }

    public static ValueType forTypeXls(CellType type, Cell cell) {
        switch (type) {
            case STRING:
                return STRING;
            case NUMERIC:
                if (DateUtil.isCellDateFormatted(cell)) {
                    return DATE;
                } else {
                    return DOUBLE;
                }
            case FORMULA:
                return STRING;
        }
        return null;
    }
}
