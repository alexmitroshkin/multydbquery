package ru.mam.multyQuery.model;

import java.util.ArrayList;
import java.util.List;

public class RowData {

    private List<CellData> cellData;

    public RowData() {
    }

    public RowData(List<CellData> cellData) {
        this.cellData = cellData;
    }

    public List<CellData> getCellData() {
        return cellData;
    }

    public void setCellData(List<CellData> cellData) {
        this.cellData = cellData;
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        List<CellData> copy = new ArrayList<>();
        for (CellData cell : cellData) {
            copy.add((CellData) cell.clone());
        }
        return new RowData(copy);
    }

}
