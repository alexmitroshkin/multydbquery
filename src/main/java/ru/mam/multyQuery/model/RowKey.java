package ru.mam.multyQuery.model;

import java.util.List;
import java.util.Objects;

/**
 *
 * @author 032MitroshkinAM
 */
public class RowKey implements Key {

    private int rowNum;
    private List<Object> keys;

    public RowKey() {
    }

    public RowKey(int rowNum, List<Object> keys) {
        this.rowNum = rowNum;
        this.keys = keys;
    }

    public int getRowNum() {
        return rowNum;
    }

    public void setRowNum(int rowNum) {
        this.rowNum = rowNum;
    }

    public List<Object> getKeys() {
        return keys;
    }

    public void setKeys(List<Object> keys) {
        this.keys = keys;
    }

    @Override
    public int hashCodeKey() {
        int hash = 7;
        hash = 89 * hash + Objects.hashCode(this.keys);
        return hash;
    }

    @Override
    public boolean equalsKey(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final RowKey other = (RowKey) obj;
        if (!Objects.equals(this.keys, other.keys)) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 89 * hash + this.rowNum;
        hash = 89 * hash + Objects.hashCode(this.keys);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final RowKey other = (RowKey) obj;
        if (this.rowNum != other.rowNum) {
            return false;
        }
        if (!Objects.equals(this.keys, other.keys)) {
            return false;
        }
        return true;
    }

}
